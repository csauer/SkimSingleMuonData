.PHONY: build

build:
	/cvmfs/cms.cern.ch/common/cmssw-env --cmsos slc6 --command-to-run sh bin/build.sh

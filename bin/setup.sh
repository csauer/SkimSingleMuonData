#!/usr/bin/env sh

# Get path of this script
PATH2SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PATH2PRJ="$( cd "$( dirname "${BASH_SOURCE[0]}" )"; cd .. >/dev/null 2>&1 && pwd )"

# Setup CMS environment
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsenv

# To access the condition database, first, set the symbolic links
ln -sfn /cvmfs/cms-opendata-conddb.cern.ch/FT53_V21A_AN6_FULL $PATH2PRJ/FT53_V21A_AN6
ln -sfn /cvmfs/cms-opendata-conddb.cern.ch/FT53_V21A_AN6_FULL.db $PATH2PRJ/FT53_V21A_AN6_FULL.db
ln -sfn /cvmfs/cms-opendata-conddb.cern.ch/FT53_V21A_AN6_FULL $PATH2PRJ/FT53_V21A_AN6_FULL

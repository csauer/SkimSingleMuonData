#!/usr/bin/env sh

# Get path of this script
PATH2SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PATH2PRJ="$( cd "$( dirname "${BASH_SOURCE[0]}" )"; cd .. >/dev/null 2>&1 && pwd )"
cd $PATH2PRJ

# Setup CMS environment
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsenv

# Buid
/cvmfs/cms.cern.ch/common/scram b

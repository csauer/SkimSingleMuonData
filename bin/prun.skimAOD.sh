#!/usr/bin/env sh

# Get path of this script
PATH2HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the current CERN user
CERN_USER=$(whoami)
DIRNAME="SkimSingleMuonData"
CMSREL="CMSSW_5_3_32"


# Write a temporary file that sets up the CMS environment CMS' open data
CMSENV="tmp.cmsenv.sh"
cat << EOF > $CMSENV
#!/usr/bin/env sh
######################################
# Set up CMS environment and release
######################################
source /cvmfs/cms.cern.ch/cmsset_default.sh
# Setup the environment if it does not exist
echo "[INFO] Setting up environment for CMS release $CMSREL"
cmsrel $CMSREL
cd $CMSREL/src
######################################
# Move project to environment
######################################
git config --global push.default matching
git clone --depth=1 --branch=master https://gitlab.com/csauer/${DIRNAME}.git
cd $DIRNAME
cmsenv
source bin/setup.sh
######################################
# Run the program
######################################
cmsRun python/skim_cfg.py
######################################
# Finalize
######################################
#mv output/skimmedAOD.root .
EOF
chmod +x $CMSENV


# Setup atlas environment for grid
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup panda

echo $CMSENV

# Submit job(s) to grid
set -x
prun --exec "/cvmfs/cms.cern.ch/common/cmssw-env --cmsos slc6 --command-to-run ./$CMSENV" --outDS user.${CERN_USER}.skimAOD.testxxx12 --nJobs 1
#prun --exec "/cvmfs/cms.cern.ch/common/cmssw-env --cmsos slc6 --command-to-run ./$CMSENV" --outDS user.${CERN_USER}.skimAOD.testxxx11 --outputs skimmedAOD.root --nJobs 1
set -x

# Delete temporary files()
#rm $CMSENV


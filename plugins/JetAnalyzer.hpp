#ifndef JETANALYZER_H
#define JETANALYZER_H

// Standard imports
#include <memory>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>

// Analysis core modules
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Common/interface/ValueMap.h"

// MiniAOD PAT libraries
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/JetReco/interface/GenJetCollection.h"

// Good old ROOT libraries
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"


class JetAnalyzer
{

  public:
    explicit JetAnalyzer(const edm::ParameterSet &);
    ~JetAnalyzer();

};

#endif /* JETANALYZER_H */

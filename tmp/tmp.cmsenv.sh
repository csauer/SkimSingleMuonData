#!/usr/bin/env sh
PATH2DIR=$(pwd)
######################################
#
# Set up CMS environment and release
#
######################################
. /cvmfs/cms.cern.ch/cmsset_default.sh
# Setup the environment if it does not exist
echo "[INFO] Setting up environment for CMS release CMSSW_5_3_32"
cmsrel CMSSW_5_3_32
cd CMSSW_5_3_32/src
######################################
#
# Move project to environment
#
######################################
git config --global push.default matching
git clone --depth=1 --branch=master https://gitlab.com/csauer/SkimSingleMuonData.git
cd SkimSingleMuonData
cmsenv
source bin/setup.sh
######################################
#
# Run the program
#
######################################
cmsRun python/skim_cfg.py
######################################
#
# Finalize
#
######################################
mv output/skimmedAOD.root $PATH2DIR/csauer.skimmedAOD.root

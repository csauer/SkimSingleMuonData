#!/usr/bin/env sh
######################################
#
# Submit job(s) to computer cluster
#
######################################
printf "Start time: "; /bin/date
printf "Job is running on node: "; /bin/hostname
printf "Job running as user: "; /usr/bin/id
printf "Job is running in directory: "; /bin/pwd
. /cvmfs/cms.cern.ch/cmsset_default.sh
cmssw-cc6 --bind /cvmfs --bind $(pwd) --command-to-run sh tmp.cmsenv.sh
echo "[INFO] Done!"

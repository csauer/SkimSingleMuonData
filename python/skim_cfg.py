import FWCore.ParameterSet.Config as cms

process = cms.Process("TEST")

process.load('Configuration.StandardSequences.Services_cff')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )
process.MessageLogger.cerr.FwkReport.reportEvery = 10
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )


## Geometry and Detector Conditions (needed for a few patTuple production steps)
process.load("Configuration.Geometry.GeometryRecoDB_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
from Configuration.AlCa.GlobalTag import GlobalTag
# Globaltag for 2012 collision data
process.GlobalTag.connect = cms.string('sqlite_file:/cvmfs/cms-opendata-conddb.cern.ch/FT53_V21A_AN6_FULL.db')
process.GlobalTag.globaltag = 'FT53_V21A_AN6::All'

# Input fiel(s)
import FWCore.Utilities.FileUtils as FileUtils
files2011data = FileUtils.loadListFromFile ('datasets/CMS_Run2012B_SingleMu_AOD_22Jan2013-v1.txt')
readFiles = cms.untracked.vstring( *files2011data )
#process.source.fileNames = readFiles

process.source = cms.Source("PoolSource",
  fileNames = readFiles,
  inputCommands=cms.untracked.vstring(
    'drop *',
    'keep recoMuons_muons_*_RECO',
    'keep recoPFMETs_pfMet_*_RECO',
    'keep recoGsfElectrons_gsfElectrons_*_RECO',
    'keep recoJetedmRefToBaseProdTofloatsAssociationVector_combinedSecondaryVertexBJetTags_*_RECO',
    'keep recoPFJets_ak5PFJets_*_RECO',
    'keep recoPFCandidates_particleFlow_*_RECO'
  ),
 dropDescendantsOfDroppedBranches=cms.untracked.bool(False)
)

# Good run list
import FWCore.ParameterSet.Config as cms
import FWCore.PythonUtilities.LumiList as LumiList
myLumis = LumiList.LumiList(filename="datasets/Cert_190456-208686_8TeV_22Jan2013ReReco_Collisions12_JSON.txt").getCMSSWString().split(",")
process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange()
process.source.lumisToProcess.extend(myLumis)

process.output = cms.OutputModule("PoolOutputModule",
  fileName = cms.untracked.string("/eos/user/c/csauer/data/CMSOpenData/SingleMuon/singleMuonSlimAOD.root")
)

process.e1 = cms.EndPath(process.output)



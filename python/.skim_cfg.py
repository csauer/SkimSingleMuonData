#!/cvmfs/cms.cern.ch/slc6_amd64_gcc472/cms/cmssw/CMSSW_5_3_32/bin/slc6_amd64_gcc472/cmsRun

# import skeleton process
from PhysicsTools.PatAlgos.patTemplate_cfg import *
from PhysicsTools.PatAlgos.tools.coreTools import *

"""
  Configuration of logger
"""

#process.options.allowUnscheduled = cms.untracked.bool(True)

"""
  Configure the source that:

  - Reads the input files
  - Links the global tag to the correct collision data
  - Initializes the list of `good` luminosity blocks
"""

process.load('Configuration.StandardSequences.Services_cff')
process.load('FWCore.MessageService.MessageLogger_cfi')

# Globaltag for 2012 collision data
process.GlobalTag.connect = cms.string('sqlite_file:/cvmfs/cms-opendata-conddb.cern.ch/FT53_V21A_AN6_FULL.db')
process.GlobalTag.globaltag = 'FT53_V21A_AN6::All'

# Good run list
import FWCore.ParameterSet.Config as cms
import FWCore.PythonUtilities.LumiList as LumiList
myLumis = LumiList.LumiList(filename='datasets/Cert_190456-208686_8TeV_22Jan2013ReReco_Collisions12_JSON.txt').getCMSSWString().split(',')
process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange()
process.source.lumisToProcess.extend(myLumis)

# Input fiel(s)
import FWCore.Utilities.FileUtils as FileUtils
files2011data = FileUtils.loadListFromFile ('datasets/CMS_Run2012B_SingleMu_AOD_22Jan2013-v1_20000_file_index.txt')
readFiles = cms.untracked.vstring( *files2011data )
#process.source.fileNames = readFiles

process.source = cms.Source("PoolSource",
  fileNames = readFiles,
#  inputCommands=cms.untracked.vstring(
#  'drop *',
#  'keep LHEEventProduct_externalLHEProducer__GEN',
#  'keep int_addPileupInfo_bunchSpacing_DIGI2RAW',
#  'keep int_mixData_bunchSpacing_DIGI2RAW',
#  'keep PileupSummaryInfos_mixData__DIGI2RAW',
#  'keep ints_genParticles__DIGI2RAW',
#  'keep recoGenJets_ak4GenJetsNoNu__DIGI2RAW',
#  'keep recoGenParticles_genParticles__DIGI2RAW',
#  'keep edmTriggerResults_TriggerResults__HLT',
#  'keep ClusterSummary_clusterSummaryProducer__RECO',
#  'keep HcalNoiseSummary_hcalnoise__RECO',
#  'keep double_fixedGridRhoAll__RECO',
#  'keep double_fixedGridRhoFastjetAll__RECO',
#  'keep double_fixedGridRhoFastjetAllTmp__RECO',
#  'keep double_fixedGridRhoFastjetCentralNeutral__RECO',
#  'keep recoDeDxHitInfosedmAssociation_dedxHitInfo__RECO',
#  'keep recoTracksToOnerecoTracksAssociation_tevMuons_dyt_RECO',
#  'keep recoTracksToOnerecoTracksAssociation_tevMuons_firstHit_RECO',
#  'keep recoTracksToOnerecoTracksAssociation_tevMuons_picky_RECO',
#  'keep recoPFTausedmRefProdrecoPFTauTransverseImpactParametersrecoPFTauTransverseImpactParameterrecoPFTauTransverseImpactParametersTorecoPFTauTransverseImpactParameteredmrefhelperFindUsingAdvanceedmRefsAssociationVector_hpsPFTauTransverseImpactParameters__RECO',
#  'keep recoJetedmRefToBaseProdrecoTracksrecoTrackrecoTracksTorecoTrackedmrefhelperFindUsingAdvanceedmRefVectorsAssociationVector_ak4JetTracksAssociatorAtVertexPF__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfCombinedCvsBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfCombinedCvsLJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfCombinedInclusiveSecondaryVertexV2BJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfCombinedMVAV2BJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfCombinedSecondaryVertexV2BJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfJetBProbabilityBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfJetProbabilityBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfSimpleInclusiveSecondaryVertexHighEffBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfSimpleSecondaryVertexHighEffBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfTrackCountingHighEffBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_softPFElectronBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_softPFMuonBJetTags__RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfDeepCSVJetTags_probb_RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfDeepCSVJetTags_probbb_RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfDeepCSVJetTags_probc_RECO',
#  'keep recoJetedmRefToBaseProdTofloatsAssociationVector_pfDeepCSVJetTags_probudsg_RECO',
#  'keep CTPPSDiamondDigiedmDetSetVector_ctppsDiamondRawToDigi_TimingDiamond_RECO',
#  'keep CTPPSDiamondLocalTrackedmDetSetVector_ctppsDiamondLocalTracks__RECO',
#  'keep CTPPSDiamondRecHitedmDetSetVector_ctppsDiamondRecHits__RECO',
#  'keep CTPPSPixelClusteredmDetSetVector_ctppsPixelClusters__RECO',
#  'keep CTPPSPixelDigiedmDetSetVector_ctppsPixelDigis__RECO',
#  'keep CTPPSPixelLocalTrackedmDetSetVector_ctppsPixelLocalTracks__RECO',
#  'keep CTPPSPixelRecHitedmDetSetVector_ctppsPixelRecHits__RECO',
#  'keep TotemRPClusteredmDetSetVector_totemRPClusterProducer__RECO',
#  'keep TotemRPDigiedmDetSetVector_totemRPRawToDigi_TrackingStrip_RECO',
#  'keep TotemRPLocalTrackedmDetSetVector_totemRPLocalTrackFitter__RECO',
#  'keep TotemRPRecHitedmDetSetVector_totemRPRecHitProducer__RECO',
#  'keep TotemRPUVPatternedmDetSetVector_totemRPUVPatternFinder__RECO',
#  'keep CSCDetIdCSCSegmentsOwnedRangeMap_cscSegments__RECO',
#  'keep DTChamberIdDTRecSegment4DsOwnedRangeMap_dt4DCosmicSegments__RECO',
#  'keep DTChamberIdDTRecSegment4DsOwnedRangeMap_dt4DSegments__RECO',
#  'keep GEMDetIdGEMRecHitsOwnedRangeMap_gemRecHits__RECO',
#  'keep RPCDetIdRPCRecHitsOwnedRangeMap_rpcRecHits__RECO',
#  'keep EcalRecHitsSorted_reducedEcalRecHitsEB__RECO',
#  'keep EcalRecHitsSorted_reducedEcalRecHitsEE__RECO',
#  'keep EcalRecHitsSorted_reducedEcalRecHitsES__RECO',
#  'keep HBHERecHitsSorted_reducedHcalRecHits_hbhereco_RECO',
#  'keep booledmValueMap_chargedHadronPFTrackIsolation__RECO',
#  'keep recoMuonMETCorrectionDataedmValueMap_muonMETValueMapProducer_muCorrData_RECO',
#  'keep recoPFCandidatesrecoPFCandidaterecoPFCandidatesrecoPFCandidateedmrefhelperFindUsingAdvanceedmRefsedmValueMap_particleBasedIsolation_gedGsfElectrons_RECO',
#  'keep recoPFCandidatesrecoPFCandidaterecoPFCandidatesrecoPFCandidateedmrefhelperFindUsingAdvanceedmRefsedmValueMap_particleBasedIsolation_gedPhotons_RECO',
#  'keep recoBeamHaloSummary_BeamHaloSummary__RECO',
#  'keep recoBeamSpot_offlineBeamSpot__RECO',
#  'keep recoCSCHaloData_CSCHaloData__RECO',
#  'keep recoGlobalHaloData_GlobalHaloData__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauChargedIsoPtSum__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauChargedIsoPtSumdR03__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByDecayModeFinding__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByDecayModeFindingNewDMs__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1DBdR03oldDMwLTraw__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1DBnewDMwLTraw__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1DBoldDMwLTraw__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1PWdR03oldDMwLTraw__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1PWnewDMwLTraw__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1PWoldDMwLTraw__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr3Hits__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseIsolationMVArun2v1PWdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseIsolationMVArun2v1PWnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseIsolationMVArun2v1PWoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByLooseMuonRejection3__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6LooseElectronRejection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6MediumElectronRejection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6TightElectronRejection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6VLooseElectronRejection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6VTightElectronRejection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6rawElectronRejection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr3Hits__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumIsolationMVArun2v1PWdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumIsolationMVArun2v1PWnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMediumIsolationMVArun2v1PWoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByPhotonPtSumOutsideSignalCone__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr3Hits__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr3Hits__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightIsolationMVArun2v1PWdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightIsolationMVArun2v1PWnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightIsolationMVArun2v1PWoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByTightMuonRejection3__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVLooseIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVLooseIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVLooseIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVLooseIsolationMVArun2v1PWdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVLooseIsolationMVArun2v1PWnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVLooseIsolationMVArun2v1PWoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVTightIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVTightIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVTightIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVTightIsolationMVArun2v1PWdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVTightIsolationMVArun2v1PWnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVTightIsolationMVArun2v1PWoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVLooseIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVLooseIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVLooseIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVTightIsolationMVArun2v1DBdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVTightIsolationMVArun2v1DBnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVTightIsolationMVArun2v1DBoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVTightIsolationMVArun2v1PWdR03oldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVTightIsolationMVArun2v1PWnewDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByVVTightIsolationMVArun2v1PWoldDMwLT__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauFootprintCorrection__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauFootprintCorrectiondR03__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauNeutralIsoPtSum__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauNeutralIsoPtSumWeight__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauNeutralIsoPtSumWeightdR03__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauNeutralIsoPtSumdR03__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauPUcorrPtSum__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauPhotonPtSumOutsideSignalCone__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauPhotonPtSumOutsideSignalConedR03__RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1DBdR03oldDMwLTraw_category_RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1DBnewDMwLTraw_category_RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1DBoldDMwLTraw_category_RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1PWdR03oldDMwLTraw_category_RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1PWnewDMwLTraw_category_RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByIsolationMVArun2v1PWoldDMwLTraw_category_RECO',
#  'keep recoPFTauDiscriminator_hpsPFTauDiscriminationByMVA6rawElectronRejection_category_RECO',
#  'keep edmErrorSummaryEntrys_logErrorHarvester__RECO',
#  'keep recoPFCandidateedmFwdPtrs_particleFlowPtrs__RECO',
#  'keep recoPFCandidateedmFwdPtrs_particleFlowTmpPtrs__RECO',
#  'keep recoCaloClusters_particleFlowEGamma_EBEEClusters_RECO',
#  'keep recoCaloClusters_particleFlowEGamma_ESClusters_RECO',
#  'keep recoCaloClusters_hybridSuperClusters_hybridBarrelBasicClusters_RECO',
#  'keep recoCaloClusters_multi5x5SuperClusters_multi5x5EndcapBasicClusters_RECO',
#  'keep recoCaloClusters_particleFlowSuperClusterECAL_particleFlowBasicClusterECALBarrel_RECO',
#  'keep recoCaloClusters_particleFlowSuperClusterECAL_particleFlowBasicClusterECALEndcap_RECO',
#  'keep recoCaloClusters_particleFlowSuperClusterECAL_particleFlowBasicClusterECALPreshower_RECO',
#  'keep recoCaloClusters_particleFlowSuperClusterOOTECAL_particleFlowBasicClusterOOTECALBarrel_RECO',
#  'keep recoCaloClusters_particleFlowSuperClusterOOTECAL_particleFlowBasicClusterOOTECALEndcap_RECO',
#  'keep recoCaloClusters_particleFlowSuperClusterOOTECAL_particleFlowBasicClusterOOTECALPreshower_RECO',
#  'keep recoCaloClusters_hybridSuperClusters_uncleanOnlyHybridBarrelBasicClusters_RECO',
#  'keep recoCaloJets_ak4CaloJets__RECO',
#  'keep recoCaloMETs_caloMet__RECO',
#  'keep recoCaloMETs_caloMetM__RECO',
#  'keep recoConversions_allConversions__RECO',
#  'keep recoConversions_conversions__RECO',
#  'keep recoConversions_particleFlowEGamma__RECO',
#  'keep recoDeDxHitInfos_dedxHitInfo__RECO',
#  'keep recoGsfElectrons_gedGsfElectrons__RECO',
#  'keep recoGsfElectronCores_gedGsfElectronCores__RECO',
#  'keep recoGsfTracks_electronGsfTracks__RECO',
#  'keep recoMuons_muons__RECO',
#  'keep recoMuons_muonsFromCosmics__RECO',
#  'keep recoMuons_muonsFromCosmics1Leg__RECO',
#  'keep recoPFCandidates_particleFlow__RECO',
#  'keep recoPFJets_ak4PFJets__RECO',
#  'keep recoPFJets_ak4PFJetsCHS__RECO',
#  'keep recoPFJets_ak8PFJetsCHS__RECO',
#  'keep recoPFMETs_pfMet__RECO',
#  'keep recoPFRecHits_particleFlowRecHitHF_Cleaned_RECO',
#  'keep recoPFTaus_hpsPFTauProducer__RECO',
#  'keep recoPFTauTransverseImpactParameters_hpsPFTauTransverseImpactParameters_PFTauTIP_RECO',
#  'keep recoPhotons_gedPhotons__RECO',
#  'keep recoPhotons_ootPhotons__RECO',
#  'keep recoPhotons_photons__RECO',
#  'keep recoPhotonCores_gedPhotonCore__RECO',
#  'keep recoPhotonCores_ootPhotonCore__RECO',
#  'keep recoPhotonCores_photonCore__RECO',
#  'keep recoRecoTauPiZeros_hpsPFTauProducer_pizeros_RECO',
#  'keep recoSuperClusters_correctedHybridSuperClusters__RECO',
#  'keep recoSuperClusters_correctedMulti5x5SuperClustersWithPreshower__RECO',
#  'keep recoSuperClusters_particleFlowEGamma__RECO',
#  'keep recoSuperClusters_particleFlowSuperClusterECAL_particleFlowSuperClusterECALBarrel_RECO',
#  'keep recoSuperClusters_particleFlowSuperClusterECAL_particleFlowSuperClusterECALEndcapWithPreshower_RECO',
#  'keep recoSuperClusters_particleFlowSuperClusterOOTECAL_particleFlowSuperClusterOOTECALBarrel_RECO',
#  'keep recoSuperClusters_particleFlowSuperClusterOOTECAL_particleFlowSuperClusterOOTECALEndcapWithPreshower_RECO',
#  'keep recoTracks_ckfInOutTracksFromConversions__RECO',
#  'keep recoTracks_ckfOutInTracksFromConversions__RECO',
#  'keep recoTracks_conversionStepTracks__RECO',
#  'keep recoTracks_cosmicMuons__RECO',
#  'keep recoTracks_cosmicMuons1Leg__RECO',
#  'keep recoTracks_generalTracks__RECO',
#  'keep recoTracks_globalMuons__RECO',
#  'keep recoTracks_standAloneMuons__RECO',
#  'keep recoTracks_standAloneMuons_UpdatedAtVtx_RECO',
#  'keep recoTracks_tevMuons_dyt_RECO',
#  'keep recoTracks_tevMuons_firstHit_RECO',
#  'keep recoTracks_tevMuons_picky_RECO',
#  'keep recoTrackExtrapolations_trackExtrapolator__RECO',
#  'keep recoVertexs_inclusiveSecondaryVertices__RECO',
#  'keep recoVertexs_offlinePrimaryVertices__RECO',
#  'keep recoVertexs_offlinePrimaryVerticesWithBS__RECO',
#  'keep recoVertexCompositeCandidates_generalV0Candidates_Kshort_RECO',
#  'keep recoVertexCompositeCandidates_generalV0Candidates_Lambda_RECO',
#  'keep recoVertexCompositePtrCandidates_inclusiveCandidateSecondaryVertices__RECO',
#  'keep recoVertexCompositePtrCandidates_inclusiveCandidateSecondaryVerticesCvsL__RECO',
#  'keep triggerTriggerEvent_hltTriggerSummaryAOD__HLT',
#      ),
# dropDescendantsOfDroppedBranches=cms.untracked.bool(False)
)

#"""
#Event selection
#"""
#
#"""
#1. Trigger
#
#Take all events that contain at least one Muon with at least p>24GeV.
#Using `andOr(True)` accepts the event if ANY trigger path fulfilling
#this requirement is fired.
#"""
#
#from HLTrigger.HLTfilters.hltHighLevel_cfi import *
#process.triggerPass = hltHighLevel.clone(
#TriggerResultsTag = "TriggerResults::HLT",
#HLTPaths = ["HLT_IsoMu24*"],
#andOr = cms.bool(True)
#)
#
#
#"""
#2. Vertex selection
#
#This is a standard selection for good-quality vertices in an event.
#
#Arguments:
#
#  isFake: If no vertex is reconstructed, a `fake` vertex is placed based on the beamline.
#          Require at least one true, i.e., reconstructed vertex.
#  nDof  : The number of degreed of freedom is closely related to the number of tracks
#          pointing to the vertex. The exact definition is given in this paper [1] where
#          nDof of the vertex fit is defined as: nDof = -3 + 2 * sum_over_track_weights.
#  abs(z): A cut on the luminous region is applied |z|<15cm(?).
#  rho   : Rho gives the curvature of the tracks pointing to the reconstructed vertex.
#          There is a cut on the maximum curvature allowed of rho<2.
#
#  ----
#
#  [1] https://arxiv.org/pdf/1405.6569.pdf
#"""
#
## vertex filter
#process.goodVertices = cms.EDFilter("VertexSelector",
#src = cms.InputTag("offlinePrimaryVertices"),
#cut = cms.string("!isFake && ndof > 4 && abs(z) < 15 && position.Rho < 2"),
#filter = cms.bool(True),
#)
#
#"""
#3. Add some jet collections.
#
#In this Section, we add some jet collections to our PATuple.
#"""
#
#"""
#- Anti-kt R=0.5 jets reconstructed from pFlow objects
#"""
#
##### load the coreTools of PAT
####from PhysicsTools.PatAlgos.tools.jetTools import *
####addJetCollection(process,
####  cms.InputTag('ak5PFJets'), 'AK5', 'PF',
####  doBTagging   = True,
####  doType1MET   = True,
####  genJetCollection = cms.InputTag("ak5GenJets"),
####  jetIdLabel   = "ak5",
####  btagInfo = ['impactParameterTagInfos','secondaryVertexTagInfos'],
####  btagdiscriminators=['simpleSecondaryVertexHighEffBJetTags','simpleSecondaryVertexHighPurBJetTags']
####)
####
##### Some minimal quality requirements on jet
####from PhysicsTools.PatAlgos.selectionLayer1.jetSelector_cfi import *
####process.goodJets = selectedPatJets.clone(src = 'ak5PFJets',
####  cut =
####  'pt > 30. &'
####  'abs(eta) < 2.4'
####)
####
##### We need jets; hence, require at least one jet
####from PhysicsTools.PatAlgos.selectionLayer1.jetCountFilter_cfi import *
####process.atLeastOneJet = countPatJets.clone(src = 'ak5PFJets', minNumber = 1)
#
#from PhysicsTools.PatAlgos.tools.jetTools import *
#switchJetCollection(process,cms.InputTag('ak5PFJets'),
#               doJTA        = True,
#               doBTagging   = True,
#               jetCorrLabel = None,
#               doType1MET   = True,
#               genJetCollection=cms.InputTag("ak5GenJets"),
#               doJetID      = True
#               )
#process.patJets.addTagInfos = True
#
#
#"""
#4. The Muon
#
#The muon is crucial for the event selection. For the time being, we only set very loose
#requirements on the selected muon; therefore, a muon that matches the following selection
#criteria is henceforth referred to as a loose muon (contrary to a tight muon which we may
#define in a later step as well).
#"""
#
#"""
#- Loose muon
#
#The loose muon is basically defined by the (loose) isolation requirement.
#
#Selection:
#
#  - We require a `global` (muon chamber tracks matched to inner tracker hits via Kalman filter technique) as well
#    as a `tracker` muon, i.e., complementary to the global-muon reconstruction, the best tracker track is used.
#  - Minimum p cut of p>20GeV (should have almost no effect due to trigger).
#  - ID acceptance cut of reconstructed pseujdorapidity.
#  - At least 10 hits in inner detector + muon chamber (14 in total)
#  - Cut on chi2 from fit (similar to number of hits)
#  - NOTE numberOfValidMuonHits: I have no clue what this is ... Hypothesis test for muon hits?
#  - dB beam line constrain. Displacement from beam line < 0.02cm(?).
#
#"""
#
#from PhysicsTools.PatAlgos.cleaningLayer1.muonCleaner_cfi import *
#process.isolatedMuons010 = cleanPatMuons.clone(preselection =
#'isGlobalMuon & isTrackerMuon &'
#'pt > 20. &'
#'abs(eta) < 2.1 &'
#'(trackIso+caloIso)/pt < 0.45 &'
#'innerTrack.numberOfValidHits > 10 &'
#'globalTrack.normalizedChi2 < 10.0 &'
#'globalTrack.hitPattern.numberOfValidMuonHits > 0 &'
#'abs(dB) < 0.02'
#)
#
##### Check overlay with previously defined jet(s)
####process.isolatedMuons010.checkOverlaps = cms.PSet(
####  jets = cms.PSet(src   = cms.InputTag("ak5PFJets"),
####    algorithm           = cms.string("byDeltaR"),
####    preselection        = cms.string(""),
####    deltaR              = cms.double(0.3),
####    checkRecoComponents = cms.bool(False),
####    pairCut             = cms.string(""),
####    requireNoOverlaps   = cms.bool(True),
####  )
####)
#
#"""
#5. MET
#"""
#
#from PhysicsTools.PatAlgos.tools.pfTools import *
#switchToPFMET(process, input=cms.InputTag('pfMet'))
#
#"""
#Remove some unnecessary information from AOD
#"""
#
#removeMCMatching(process, ['All'])
##removeAllPATObjectsBut(process, ['Muons','Jets', 'METs'])

"""
Run paths
"""

#process.p = cms.Path(
#  process.patDefaultSequence
##  process.goodVertices *
##  process.isolatedMuons010
##  process.atLeastOneJet
#)

"""
  Output
"""

#from PhysicsTools.PatAlgos.patEventContent_cff import patEventContentNoCleaning
#process.out.outputCommands = cms.untracked.vstring(
#    'keep recoJetdmRefProdTofloatAssociationVector_jetProbabilityBJetTags__RECO'
###  'drop *',
###  'keep patMETs_patMETs__PAT',
###  'keep recoPFCandidates_particleFlow_*_*',
###  'keep *_offlinePrimaryVertices_*_*',
###  'keep recoPFCandidates_particleFlow_*_*',
###  'keep recoGenParticles_genParticles*_*_*',
###  'keep GenEventInfoProduct_*_*_*',
###  'keep GenRunInfoProduct_*_*_*',
###  'keep edmTriggerResults_TriggerResults_*_HLT',
###  'keep *_pat*_*_*'
#)

process.maxEvents.input = 100
# output file
process.out.fileName = 'output/skimmedAOD.root'

process.e1 = cms.EndPath(process.out)
